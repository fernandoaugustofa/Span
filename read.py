import re
def clean_list(lista):
    nova_lista = []
    for item in lista:
        if(len(item)>2) and not (item.isdigit()):
            nova_lista.append(item.lower())
    return nova_lista

def database(path):
    arq = open(r""+path)
    mail_list = []
    for row in arq:
        mail_list.append(re.split(r'\W+', row))

    mail_list_clean = []
    for mail in mail_list:
        mail_list_clean.append(clean_list(mail))

    return mail_list_clean
