import read
import re
import os

def read_base_spam():
    spam = open(r"" + os.getcwd()+ "\\spam_data.txt")
    not_spam = open(r"" + os.getcwd()+ "\\not_spam_data.txt")
    
    total_s = 0
    total_ns = 0
    
    data_spam = {}
    for word in spam:
        words_split = word.split(':')
        try:
            data_spam[words_split[0]]["ocorr"] = words_split[1]
            data_spam[words_split[0]]["med"] = words_split[2]
            data_spam[words_split[0]]["freq"] = words_split[3]
            data_spam[words_split[0]]["relev"] = words_split[4]
            total_s +=1
        except:
            total_s +=0

    data_not_spam = {}
    for word in not_spam:
        words_split = word.split(':')
        try:
            data_not_spam[words_split[0]]["ocorr"] = words_split[1]
            data_not_spam[words_split[0]]["med"] = words_split[2]
            data_not_spam[words_split[0]]["freq"] = words_split[3]
            data_not_spam[words_split[0]]["relev"] = words_split[4]
            total_ns +=1
        except:
            total_s +=0

    spam.close()
    not_spam.close()
    return [data_spam, data_not_spam, total_s, total_ns]

def write_base_spam(data_spam, data_not_spam):
    spam = open(r"" + os.getcwd()+ "\\spam_data.txt", 'w')
    not_spam = open(r"" + os.getcwd()+ "\\not_spam_data.txt", 'w')
    text = str((len(data_spam))) + "\n"
    for word in data_spam.keys():
        #print(text)
        text = text + word +':'+ str(data_spam[word]["ocorr"]) + ':'
        text = text + str(data_spam[word]["med"]) + ':'
        text = text + str(data_spam[word]["freq"]) + ':'
        text = text + str(data_spam[word]["relev"]) + "\n"
    spam.writelines(text)
                          

    #spam.writelines(text)
        #print(word)
        #print(data_spam[word]["ocorr"])
        #print(data_spam[word]["med"])
        #print(data_spam[word]["freq"])
        #print(data_spam[word]["relev"])
        #text = text + word + ':' + str(data_spam[word]["ocorr"]) + ':' str(data_spam[word]["med"]) + ':' str(data_spam[word]["freq"]) + ':' +  str(data_spam[word]["relev"]) + "\n"
    #spam.writelines(text)

    text = str((len(data_not_spam))) + "\n"
    for word in data_not_spam.keys():
        text = text + word +':'+ str(data_not_spam[word]["ocorr"]) + ':'
        text = text + str(data_not_spam[word]["med"]) + ':'
        text = text + str(data_not_spam[word]["freq"]) + ':'
        text = text + str(data_not_spam[word]["relev"]) + "\n"
    not_spam.writelines(text)
        #text = text + word + ":" + str(data_not_spam[word]["ocorr"]) + ":" str(data_not_spam[word]["med"]) + ":" str(data_not_spam[word]["freq"]) + ":" +  str(data_not_spam[word]["relev"]) + "\n"
    #not_spam.writelines(text)
    spam.close()
    not_spam.close()
    
def atualize(path):
    mail_list = read.database(r""+path)
    #aqui eu vou pegar os dados que eu mesmo escrevo
    [spam_data, not_spam_data, total_spam, total_not_spam] = read_base_spam()
    #spam_data = {}
    #not_spam_data = {}
    #total_spam = 0
    #total_not_spam = 0
    #for mail in mail_list:
        #for word in mail:
            #spam_data[word] = {"ocorr" :0, "med" :0, "freq":0, "relev":0}
            #not_spam_data[word] = {"ocorr" :0, "med" :0, "freq":0, "relev":0}
    for mail in mail_list:
        for i in range(len(mail)):
            if(i!=0):
                if(mail[0]=="spam"):
                    if(mail[i] in spam_data.keys()):
                        total_spam +=1
                        spam_data[mail[i]]["ocorr"] += 1
                    else:
                        spam_data[mail[i]] = {"ocorr" :1, "med" :0, "freq":0, "relev":0}
                elif(mail[0]=="ham"):
                    if(mail[i] in not_spam_data.keys()):
                        total_not_spam += 1
                        not_spam_data[mail[i]]["ocorr"] += 1
                    else:
                        not_spam_data[mail[i]] = {"ocorr" :1, "med" :0, "freq":0, "relev":0}

    write_base_spam(spam_data, not_spam_data)
    #aqui eu vou atualizar os dados num arquivo

def isSpam(text_mail):
    #aqui eu vou ler os dados
    spam_data, not_spam_data, total_spam, total_not_spam = read_base_spam()
    mail = text_mail
    list_word = re.split(r'\W+', mail)
    w = read.clean_list(list_word)
    freq_spam = 0
    freq_not_spam = 0

    for word in w:
        try:
            if(spam_data[word]["ocorr"]>=not_spam_data[word]["ocorr"]):
                freq_spam +=1
            else:
                freq_not_spam +=1
        except:
            freq_not_spam +=1

    if freq_spam > freq_not_spam:
        return 1
    else:
        return 0
    
